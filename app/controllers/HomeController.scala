package controllers

import akka.actor.ActorSystem
import com.sample.poc.Printable
import controllers.action.MyActionBuilder
import play.api.mvc._

import scala.concurrent.ExecutionContext

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
class HomeController(action: MyActionBuilder, cc: ControllerComponents)
                    (implicit val executionContext: ExecutionContext, actorSystem: ActorSystem) extends AbstractController(cc) with Printable {

  
  /**
    * Create an Action to render an HTML page.
    *
    * The configuration in the `routes` file means that this method
    * will be called when the application receives a `GET` request with
    * a path of `/`.
    */
  def index(): Action[AnyContent] = Action {
    Printer("hello world")
    Ok(views.html.index())
  }
}
