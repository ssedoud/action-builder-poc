package controllers.action

import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

case class EnrichedRequest[A](request: Request[A]) extends WrappedRequest[A](request)

class MyActionBuilder (implicit val defExecutionContext: ExecutionContext, anyContentBodyParser: BodyParser[AnyContent])
  extends ActionBuilder[EnrichedRequest, AnyContent] {

  override def parser: BodyParser[AnyContent] = anyContentBodyParser

  override def invokeBlock[A](request: Request[A], block: (EnrichedRequest[A]) => Future[Result]): Future[Result] = block(EnrichedRequest(request))

  override protected def executionContext: ExecutionContext = defExecutionContext
}
