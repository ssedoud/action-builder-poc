package filters

import play.api.Environment
import play.api.http.HttpFilters
import play.api.mvc.EssentialFilter

import scala.concurrent.ExecutionContext

class ApplicationFilters(env: Environment,
                         loggingContextFilter: LoggingContextFilter, requestTimeLoggerFilter: RequestTimeLoggerFilter)(implicit val
executionContext: ExecutionContext) extends HttpFilters {

  /**
    * Ordered list of `play.api.mvc.Filters` that run on every request.
    */
  override val filters: Seq[EssentialFilter] = Seq(loggingContextFilter, requestTimeLoggerFilter)
}
