package filters

import akka.stream.Materializer
import org.slf4j.MDC
import play.api.Logger
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.Future

class LoggingContextFilter(implicit val mat: Materializer) extends Filter {

  protected lazy val logger = Logger(this.getClass)

  override def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {

    // Propagating MDC map for logging, from thread to thread for a single request, using the custom dispatcher
    logger.info("Building the MDC infos for consistent logging")
    MDC.put("ip", requestHeader.remoteAddress)

    // Next Filter call
    nextFilter(requestHeader)
    
  }
}