package filters

import akka.stream.Materializer
import play.api.Logger
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.Future

class RequestTimeLoggerFilter (implicit val mat: Materializer) extends Filter {

  protected lazy val logger = Logger(this.getClass)

  override def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    
    logger.info("request log")


    // Next Filter call
    nextFilter(requestHeader)
  }
}