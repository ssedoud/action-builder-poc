package filters

import java.util.concurrent.TimeUnit

import akka.dispatch._
import com.typesafe.config.Config
import org.slf4j.MDC

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{Duration, FiniteDuration}

/**
  * Configurator for a MDC map propagating dispatcher
  *
  * Credits to James Roper for [[https://github.com/jroper/thread-local-context-propagation/ initial implementation]]
  * and Yann Simon for [[http://yanns.github.io/blog/2014/05/04/slf4j-mapped-diagnostic-context-mdc-with-play-framework/
  * practical example]]
  */
class MDCPropagatingDispatcherConfigurator(config: Config, prerequisites: DispatcherPrerequisites)
  extends MessageDispatcherConfigurator(config, prerequisites) {

  private val instance = new MDCPropagatingDispatcher(
    this,
    config.getString("id"),
    config.getInt("throughput"),
    FiniteDuration(config.getDuration("throughput-deadline-time", TimeUnit.NANOSECONDS), TimeUnit.NANOSECONDS),
    configureExecutor(),
    FiniteDuration(config.getDuration("shutdown-timeout", TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS))

  override def dispatcher(): MessageDispatcher = instance
}

/**
  * Custom dispatcher to propagate the MDC map
  *
  * This dispatcher either propagates or cleares the MDC map through the current request threads,
  * depending of the status of the MDC
  */
class MDCPropagatingDispatcher(
                                _configurator: MessageDispatcherConfigurator,
                                id: String,
                                throughput: Int,
                                throughputDeadlineTime: Duration,
                                executorServiceFactoryProvider: ExecutorServiceFactoryProvider,
                                shutdownTimeout: FiniteDuration)
  extends Dispatcher(_configurator,
    id,
    throughput,
    throughputDeadlineTime,
    executorServiceFactoryProvider,
    shutdownTimeout) {
  self =>

  override def prepare(): ExecutionContext = new ExecutionContext {

    // capture the MDC
    val mdcContext = MDC.getCopyOfContextMap

    override def execute(r: Runnable) = self.execute(new Runnable {

      override def run() = {
        // backup the callee MDC context
        val oldMDCContext = MDC.getCopyOfContextMap

        // Run the runnable with the captured context
        setContextMap(mdcContext)
        try {
          r.run()
        } finally {
          // restore the callee MDC context
          setContextMap(oldMDCContext)
        }
      }
    })

    override def reportFailure(t: Throwable) = self.reportFailure(t)
  }

  /**
    * Copies or cleares the MDC map with a given context
    *
    * @param context The given context
    */
  private[this] def setContextMap(context: java.util.Map[String, String]) {

    // Wrapping context to avoid null values
    Option(context) match {
      case Some(`context`) => MDC.setContextMap(context)
      case _ => MDC.clear()
    }
  }
}
