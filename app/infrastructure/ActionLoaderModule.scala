package infrastructure

import com.softwaremill.macwire.wire
import controllers.action.MyActionBuilder
import play.api.BuiltInComponents
import play.api.mvc.{AnyContent, BodyParser}

trait ActionLoaderModule extends BuiltInComponents {
  
  private implicit lazy val anyContentBodyParser: BodyParser[AnyContent] = defaultBodyParser

  lazy val myActionBuilder = wire[MyActionBuilder]

}
