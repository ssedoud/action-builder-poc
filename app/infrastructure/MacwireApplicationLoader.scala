package infrastructure

import com.softwaremill.macwire.wire
import controllers.{AssetsComponents, HomeController}
import filters.{ApplicationFilters, LoggingContextFilter, RequestTimeLoggerFilter}
import play.api.ApplicationLoader.Context
import play.api.mvc.{AnyContent, BodyParser, EssentialFilter}
import play.api.routing.Router
import play.api.{Application, ApplicationLoader, BuiltInComponentsFromContext, LoggerConfigurator}
import router.Routes


class MacwireApplicationLoader extends ApplicationLoader {
  override def load(context: Context): Application = new ApplicationComponents(context).application
}

sealed class ApplicationComponents(context: Context) extends BuiltInComponentsFromContext(context)
  with ActionLoaderModule
  with AssetsComponents {

  
  lazy val loggingContextFilter: LoggingContextFilter = wire[LoggingContextFilter]
  lazy val requestTimeLoggerFilter: RequestTimeLoggerFilter = wire[RequestTimeLoggerFilter]
  
  // Logger
  LoggerConfigurator(context.environment.classLoader).foreach(loggerConfigurator =>
    loggerConfigurator.configure(context.environment))

  // Request body parser made implicit
  implicit val anyContentBodyParser: BodyParser[AnyContent] = defaultBodyParser
  implicit val system = actorSystem


  //Controller injection
  lazy val homeController: HomeController = wire[HomeController]

  // Routes
  override lazy val router: Router = {
    // add the prefix string in local scope for the Routes constructor
    val prefix: String = "/"
    wire[Routes]
  }

  override lazy val httpFilters: Seq[EssentialFilter] = wire[ApplicationFilters].filters

}