// Repositories
resolvers ++= Seq(
  "Typesafe" at "https://repo.typesafe.com/typesafe/releases"
)

// Plugins
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.3")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.2.1")
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "0.9.0")
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.0")
addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.3.0")
addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.0.0-RC9")





