
name := """action-builder-poc"""
organization := "com.sample.poc"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
  // DI with MacWire
  "com.softwaremill.macwire" %% "macros" % "2.3.0" % Provided,
  "com.softwaremill.macwire" %% "util" % "2.3.0" % Compile,
  "com.softwaremill.macwire" %% "proxy" % "2.3.0" % Compile,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.0" % Test,
  "com.typesafe.play" %% "play-json" % "2.6.3",
  "com.typesafe.akka" %% "akka-actor" % "2.5.4",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.4",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.sample.poc" %% "actor-lib-poc" % "1.1-SNAPSHOT" changing()
)

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.sample.poc.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.sample.poc.binders._"
